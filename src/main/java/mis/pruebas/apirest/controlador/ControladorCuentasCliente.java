package mis.pruebas.apirest.controlador;

import mis.pruebas.apirest.modelos.Cliente;
import mis.pruebas.apirest.modelos.Cuenta;
import mis.pruebas.apirest.modelos.CuentasCliente;
import mis.pruebas.apirest.servicios.ServicioCliente;
import mis.pruebas.apirest.servicios.ServicioCuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping(Rutas.CLIENTES + "/{documento}/cuentas")
public class ControladorCuentasCliente {

    // CRUD - GET *,GET,POST,PUT,PATCH

    @Autowired
    ServicioCliente servicioCliente;

    @Autowired
    ServicioCuenta servicioCuenta;

    public static class DatosEntradaCuenta {
        public String codigoCuenta;
    };

    // POST http://localhost:9000/api/v1/clientes/12345678/cuentas + DATOS -> agregarCuentaCliente(documento,cuenta)
    @PostMapping
    public ResponseEntity agregarUnaCuentaCliente(@PathVariable String documento,
                                               @RequestBody DatosEntradaCuenta datosEntradaCuenta) {
        try {
            this.servicioCliente.agregarCuentaCliente(documento, datosEntradaCuenta.codigoCuenta);
        } catch(RuntimeException x) {
            System.err.println(x);
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }


    // GET http://localhost:9000/api/v1/clientes/12345678/cuentas -> obtenerCuentasCliente(documento)
    @GetMapping
    public ResponseEntity<CuentasCliente> obtenerCuentasCliente(@PathVariable String documento) {
        try {
            final var cuentasCliente = this.servicioCliente.obtenerCuentasCliente(documento);
            return ResponseEntity.ok(cuentasCliente);
        } catch(RuntimeException x) {
            return ResponseEntity.notFound().build();
        }
    }


    // GET http://localhost:9000/api/v1/clientes/12345678/cuentas/093840394 -> obtenerCuentaCliente(documento,numeroCuenta)
    @GetMapping("/{numeroCuenta}")
    public ResponseEntity<Cuenta> obtenerUnaCuentaUnCliente(@PathVariable String documento, @PathVariable String numeroCuenta) {
        try {
            final var cuenta = this.servicioCuenta.obtenerUnaCuentaUnCliente(documento, numeroCuenta);
            return ResponseEntity.ok(cuenta);
        } catch (RuntimeException x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    // DELETE http://localhost:9000/api/v1/clientes/12345678/cuentas/093840394 -> eliminarCuentaCliente(documento,numeroCuenta)
    @DeleteMapping("/{numeroCuenta}")
    public ResponseEntity eliminarUnaCuentaCliente(@PathVariable String documento, @PathVariable String numeroCuenta) {
        try {
            this.servicioCuenta.eliminarCuentaCliente(documento, numeroCuenta);
        } catch(RuntimeException x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.noContent().build();
    }
}
