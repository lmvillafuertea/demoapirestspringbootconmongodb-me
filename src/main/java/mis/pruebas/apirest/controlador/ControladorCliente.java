package mis.pruebas.apirest.controlador;

import mis.pruebas.apirest.modelos.Cliente;
import mis.pruebas.apirest.servicios.ServicioCliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(Rutas.CLIENTES)
public class ControladorCliente {

    @Autowired
    ServicioCliente servicioCliente;

    @Autowired
    PagedResourcesAssembler pagedResourcesAssembler;

    // GET http://localhost:9000/api/v1/clientes?pagina=7&cantidad=3 -> List<Cliente> obtenerClientes()
    @GetMapping
    //public PagedModel<EntityModel<Cliente>> obtenerClientes (@RequestParam int pagina, @RequestParam int cantidad){
    public List<Cliente> obtenerClientes(@RequestParam int pagina, @RequestParam int cantidad) {
        try {
            /*final var pageSinMetadatos = this.servicioCliente.obtenerClientes(pagina,cantidad);
            final var page = pageSinMetadatos.map(
                    x -> EntityModel.of(x).add(
                            linkTo(methodOn(this.getClass()).obtenerUnCliente(x.documento))
                                    .withSelfRel().withTitle("Este cliente"),
                            linkTo(methodOn(ControladorCuentasCliente.class).obtenerCuentasCliente(x.documento))
                                    .withRel("cuentas").withTitle("Cuentas del cliente")
                    ));*/
            return this.servicioCliente.obtenerClientes(pagina - 1, cantidad);
            //return this.pagedResourcesAssembler.toModel(page);
        } catch(Exception x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }


    // POST http://localhost:9000/api/v1/clientes + DATOS -> agregarCliente(Cliente)
    // 200 OK, Location: http://localhost:9000/api/v1/clientes/109343498
    @PostMapping
    public void crearCliente(@RequestBody Cliente cliente) {

        this.servicioCliente.insertarClienteNuevo(cliente);
    /*public ResponseEntity agregarCliente(@RequestBody Cliente cliente) {

        this.servicioCliente.insertarClienteNuevo(cliente);


        // Link { rel, url }
        // obtenerUnCliente(documento) -> http://localhost:9000/api/v1/clientes/109343498 (Link {rel,url})
        // Link.url -> Location


        // Obtengo un objeto que representa una invocación al método del controlador obtenerUnCliente("109343498").
        final var representacionMetodoObtenerUnClienteConDocumento = methodOn(ControladorCliente.class)
                .obtenerUnCliente(cliente.documento);

        // Con la representación de la invocación, obtengo un enlace.
        final var enlaceEsteDocumento = linkTo(representacionMetodoObtenerUnClienteConDocumento).toUri();
        // URI "http://localhost:9000/api/v1/clientes/109343498"

        // Patrón de diseño: Builder
        return ResponseEntity
                .ok() // 200 OK
                .location(enlaceEsteDocumento) // Location: http://localhost:9000/api/v1/clientes/109343498
                .build(); // Mandar la resupesta.
        */
    }

    // GET http://localhost:9000/api/v1/clientes/{documento} -> obtenerUnCliente(documento)
    // GET http://localhost:9000/api/v1/clientes/12345678 -> obtenerUnCliente("12345678")
    @GetMapping("/{documento}")
    public Cliente obtenerUnCliente(@PathVariable String documento) {
        try {
            return this.servicioCliente.obtenerUnCliente(documento);
        } catch(Exception x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    // PUT http://localhost:9000/api/v1/clientes/{documento} + DATOS -> reemplazarUnCliente(documento, DATOS)
    // PUT http://localhost:9000/api/v1/clientes/12345678 + DATOS -> reemplazarUnCliente("12345678", DATOS)
    @PutMapping("/{documento}")
    public void reemplazarUnCliente(@PathVariable("documento") String nroDocumento, @RequestBody Cliente cliente) {
        try {
            cliente.documento = nroDocumento;
            this.servicioCliente.guardarCliente(cliente);
        } catch(Exception x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    // PUT http://localhost:9000/api/v1/clientes/{documento} + DATOS -> emparcharUnCliente(documento, DATOS)
    // PUT http://localhost:9000/api/v1/clientes/12345678 + DATOS -> emparcharUnCliente("12345678", DATOS)
    @PatchMapping("/{documento}")
    public void emparcharUnCliente(@PathVariable("documento") String nroDocumento, @RequestBody Cliente cliente) {
        /*cliente.documento = nroDocumento;
        this.servicioCliente.emparcharCliente(cliente);
         */
        try{
            cliente.documento = nroDocumento;
            //this.servicioCliente.guardarCliente(cliente);
            this.servicioCliente.emparcharUnCliente(cliente);
        } catch (Exception x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    // DELETE http://localhost:9000/api/v1/clientes/{documento} -> eliminarUnCliente(documento)
    // DELETE http://localhost:9000/api/v1/clientes/12345678 + DATOS -> eliminarUnCliente("12345678")
    @DeleteMapping("/{documento}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void eliminarUnCliente(@PathVariable String documento) {
        try {
            this.servicioCliente.borrarCliente(documento);
        } catch(Exception x) {}
    }
}
