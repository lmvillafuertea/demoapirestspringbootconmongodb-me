package mis.pruebas.apirest.controlador;


import mis.pruebas.apirest.modelos.Cuenta;
import mis.pruebas.apirest.servicios.ServicioCuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping(Rutas.CUENTAS)

public class ControladorCuenta {

    @Autowired
    ServicioCuenta servicioCuenta;

    // http://localhost:9000/api/v1/cuentas
    @GetMapping
    public List<Cuenta> obtenerCuentas(){
        return this.servicioCuenta.obtenerCuentas();
    }

    // http://localhost:9000/api/v1/cuentas + DATOS
    @PostMapping
    public void crearCuenta(@RequestBody Cuenta cuenta){
        this.servicioCuenta.insertarCuentaNueva(cuenta);
    }

    // http://localhost:9000/api/v1/cuentas/{numero}
    // http://localhost:9000/api/v1/cuentas/12345678
    @GetMapping("/{numero}")
    public Cuenta obtenerUnaCuenta(@PathVariable String numero) {
        return this.servicioCuenta.buscarUnaCuenta(numero);
    }

    // http://localhost:9000/api/v1/cuentas/{numero} + DATOS
    // http://localhost:9000/api/v1/cuentas/12345678 + DATOS
    @PutMapping("/{numero}")
    public void reemplazarUnaCuenta(@PathVariable("numero") String ctanumero,@RequestBody Cuenta cuenta){
        try {
            cuenta.numero = ctanumero;
            this.servicioCuenta.guardarCuenta(cuenta);
        } catch(RuntimeException x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

    }

    // http://localhost:9000/api/v1/cuentas/{numero} + DATOS
    // http://localhost:9000/api/v1/cuentas/12345678 + DATOS
    @PatchMapping("/{numero}")
    public void emparcharUnaCuenta(@PathVariable("numero") String ctanumero,@RequestBody Cuenta cuenta) {
        cuenta.numero = ctanumero;
        this.servicioCuenta.emparcharUnaCuenta(cuenta);
    }

    // DELETE http://localhost:9000/api/v1/cuentas/{numero}
    // DELETE http://localhost:9000/api/v1/cuentas/12345678 + DATOS
    @DeleteMapping("/{numero}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borrarunaCuenta(@PathVariable String numero) {
        try{
            this.servicioCuenta.borrarCuenta(numero);
        } catch (Exception x) {
        }
    }

}
