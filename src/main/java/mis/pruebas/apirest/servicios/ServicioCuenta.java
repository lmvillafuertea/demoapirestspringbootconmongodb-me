package mis.pruebas.apirest.servicios;

import mis.pruebas.apirest.modelos.Cuenta;

import java.util.List;

public interface ServicioCuenta {

    public List<Cuenta> obtenerCuentas();

    //CREAR
    public void insertarCuentaNueva(Cuenta cuenta);

    //READ
    public Cuenta buscarUnaCuenta(String numero);

    //UPDATE
    public void guardarCuenta(Cuenta cuenta);
    public void emparcharUnaCuenta(Cuenta parche);

    //DELETE
    public  void borrarCuenta(String numero);

    public Cuenta obtenerUnaCuentaUnCliente(String documento, String numeroCuenta);

    public void eliminarCuentaCliente(String documento, String numeroCuenta);

}
