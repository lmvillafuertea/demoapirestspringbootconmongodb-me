package mis.pruebas.apirest.servicios.impl;

import mis.pruebas.apirest.modelos.Cliente;
import mis.pruebas.apirest.modelos.CuentasCliente;
import mis.pruebas.apirest.servicios.ServicioCliente;
import mis.pruebas.apirest.servicios.repositorios.RepositorioCliente;
import mis.pruebas.apirest.servicios.repositorios.RepositorioCuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.*;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import static org.springframework.data.mongodb.core.query.Query.*;
import static org.springframework.data.mongodb.core.query.Criteria.*;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;

@Service
public class ServicioClienteImpl implements ServicioCliente {

    @Autowired
    RepositorioCliente repositorioCliente;

    public final Map<String,Cliente> clientes = new ConcurrentHashMap<String,Cliente>();

    @Override
    //public Page<Cliente> obtenerClientes(int pagina, int cantidad) {
    public List<Cliente> obtenerClientes(int pagina, int cantidad) {
        final List<Cliente> clientesContados = List.copyOf(this.clientes.values());
        int indiceIncial = pagina*cantidad;
        int indiceFinal = indiceIncial + cantidad;
        System.out.println((clientesContados.size()));

        if(indiceFinal > clientesContados.size()){
            indiceFinal = clientesContados.size();
        }
        System.out.println("Indice inicial : " + indiceIncial + "Indice final: " + indiceFinal);
        //return clientesContados.subList(indiceIncial, indiceFinal);
        return this.repositorioCliente.findAll();
        //return this.repositorioCliente.findAll(PageRequest.of(pagina,cantidad));
    }

    @Override
    public void insertarClienteNuevo(Cliente cliente) {
        //this.clientes.put(cliente.documento, cliente);
        this.repositorioCliente.insert(cliente);
        System.err.println(String.format("==== insertarClienteNuevo %s = %s ","documento:", cliente.documento));
        System.err.println(String.format("==== insertarClienteNuevo %s = %s ","nombre:", cliente.nombre));
        System.err.println(String.format("==== insertarClienteNuevo %s = %s ","edad:", cliente.edad));
        System.err.println(String.format("==== insertarClienteNuevo %s = %s ","correo:", cliente.correo));
        System.err.println(String.format("==== insertarClienteNuevo %s = %s ","direccion:", cliente.direccion));
        System.err.println(String.format("==== insertarClienteNuevo %s = %s ","telefono:", cliente.telefono));
        System.err.println("************************************************");
    }

    @Override
    public Cliente obtenerUnCliente(String documento) {
        /*if (!this.clientes.containsKey(documento))
            throw new RuntimeException("No existe el cliente: " + documento);
        return this.clientes.get(documento);
        */
        final Optional<Cliente> quizasCliente = this.repositorioCliente.findById(documento);
        if (!quizasCliente.isPresent())
            throw new RuntimeException("No existe el cliente con documento " + documento);
        return quizasCliente.get();
    }

    @Override
    public void guardarCliente(Cliente cliente) {
        /*if (!this.clientes.containsKey(cliente.documento))
            throw new RuntimeException("No existe el cliente: " + cliente.documento);

        // No dejo reemplazar las cuentas del cliente.
        final var cuentasCliente = obtenerCuentasCliente(cliente.documento);
        cliente.codigosCuentas = cuentasCliente;

        // "Piso" el objeto en el hashmap.
        this.clientes.replace(cliente.documento, cliente);
         */
        if (!this.repositorioCliente.existsById(cliente.documento))
            throw new RuntimeException("No existe el cliente con documento " + cliente.documento);
        this.repositorioCliente.save(cliente);
    }

    @Autowired
    MongoOperations mongoOperations;

    @Override
    public void emparcharUnCliente(Cliente parche) {
        final Query query = query(where("_id").is(parche.documento));
        final Update update = new Update();

        set(update,"nombre",parche.nombre);
        set(update,"edad",parche.edad);
        set(update,"fechaNacimiento",parche.fechaNacimiento);
        set(update,"correo",parche.correo);
        set(update,"telefono",parche.telefono);
        set(update,"direccion",parche.direccion);
        System.err.println("************************************************");
        mongoOperations.updateFirst(query,update,"cliente");

        /*final Cliente existente = this.clientes.get(parche.documento);

        if(parche.edad != existente.edad)
            existente.edad = parche.edad;

        if(parche.nombre != null)
            existente.nombre = parche.nombre;

        if(parche.correo != null)
            existente.correo = parche.correo;

        if(parche.direccion != null)
            existente.direccion = parche.direccion;

        if(parche.telefono != null)
            existente.telefono = parche.telefono;

        if(parche.fechaNacimiento != null)
            existente.fechaNacimiento = parche.fechaNacimiento;

        this.clientes.replace(existente.documento, existente);
        */
    }

    private void set(Update update, String nombre, Object valor) {
        if(valor != null) {
            System.err.println(String.format("==== MODIFICAR CAMPO %s = %s", nombre, valor));
            update.set(nombre, valor);
        }
    }

    @Override
    public void borrarCliente(String documento) {
        this.repositorioCliente.deleteById(documento);
        System.out.println(String.format("==== borrarCliente Documento Eliminado = %s", documento));
        System.err.println("************************************************");
        /*if (!this.clientes.containsKey(documento))
            throw new RuntimeException("No existe el cliente: " + documento);
        this.clientes.remove(documento);
        */
    }

    @Autowired
    RepositorioCuenta repositorioCuenta;

    @Override
    public void agregarCuentaCliente(String documento, String numeroCuenta) {
        final Cliente cliente = this.obtenerUnCliente(documento);
        //cliente.codigosCuentas.add(numeroCuenta);
        if(!this.repositorioCuenta.existsById(numeroCuenta))
            throw new RuntimeException("No existe la cuenta número " + numeroCuenta);

        System.out.println(String.format("==== agregarCuentaCliente %s = %s", documento, numeroCuenta));
        System.err.println("************************************************");

        cliente.codigosCuentas.add(numeroCuenta);
        this.repositorioCliente.save(cliente);
    }

    @Override
    /*public List<String> obtenerCuentasCliente(String documento) {
        final Cliente cliente = this.obtenerUnCliente(documento);
        return cliente.codigosCuentas;
    }*/
    public CuentasCliente obtenerCuentasCliente(String documento) {
        return this.repositorioCliente.obtenerCuentasCliente(documento);
    }
}
