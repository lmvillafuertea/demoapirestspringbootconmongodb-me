package mis.pruebas.apirest.servicios.impl;

import mis.pruebas.apirest.modelos.Cuenta;
import mis.pruebas.apirest.servicios.ServicioCliente;
import mis.pruebas.apirest.servicios.ServicioCuenta;
import mis.pruebas.apirest.servicios.repositorios.RepositorioCuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.*;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static org.springframework.data.mongodb.core.query.Criteria.*;
import static org.springframework.data.mongodb.core.query.Query.*;

@Service
public class ServicioCuentaImpl implements ServicioCuenta {

    public final Map<String,Cuenta> cuentas = new ConcurrentHashMap<String,Cuenta>();

    @Autowired
    RepositorioCuenta repositorioCuenta;

    @Override
    public List<Cuenta> obtenerCuentas() {
        //return List.copyOf(this.cuentas.values());
        return this.repositorioCuenta.findAll();
    }

    @Override
    public void insertarCuentaNueva(Cuenta cuenta) {
        //this.cuentas.put(cuenta.numero,cuenta) ;
        this.repositorioCuenta.insert(cuenta);
        System.err.println(String.format("==== insertarCuentaNueva %s = %s ","numero:", cuenta.numero));
        System.err.println(String.format("==== insertarCuentaNueva %s = %s ","moneda:", cuenta.moneda));
        System.err.println(String.format("==== insertarCuentaNueva %s = %s ","saldo:", cuenta.saldo));
        System.err.println(String.format("==== insertarCuentaNueva %s = %s ","tipo:", cuenta.tipo));
        System.err.println(String.format("==== insertarCuentaNueva %s = %s ","estado:", cuenta.estado));
        System.err.println(String.format("==== insertarCuentaNueva %s = %s ","oficina:", cuenta.oficina));
        System.err.println("************************************************");
    }

    @Override
    public Cuenta buscarUnaCuenta(String numero) {
        //return this.cuentas.get(numero);
        final var quizasCuenta = this.repositorioCuenta.findById(numero);
        if (!quizasCuenta.isPresent())
            throw new RuntimeException("No existe la cuenta número " + numero);
        System.err.println(String.format("==== buscarUnaCuenta %s = %s ","numero:", numero));
        System.err.println("************************************************");
        return quizasCuenta.get();
    }

    @Override
    public void guardarCuenta(Cuenta cuenta) {
        //this.cuentas.replace(cuenta.numero, cuenta);
        if (!this.repositorioCuenta.existsById(cuenta.numero))
            throw new RuntimeException("No existe la cuenta número " + cuenta.numero);
        this.repositorioCuenta.save(cuenta);

        System.err.println(String.format("==== guardarCuenta %s = %s ","numero:", cuenta.numero));
        System.err.println(String.format("==== guardarCuenta %s = %s ","moneda:", cuenta.moneda));
        System.err.println(String.format("==== guardarCuenta %s = %.2f ","saldo:", cuenta.saldo));
        System.err.println(String.format("==== guardarCuenta %s = %s ","tipo:", cuenta.tipo));
        System.err.println(String.format("==== guardarCuenta %s = %s ","estado:", cuenta.estado));
        System.err.println(String.format("==== guardarCuenta %s = %s ","oficina:", cuenta.oficina));
        System.err.println("************************************************");
    }

    @Autowired
    MongoOperations mongoOperations;

    @Override
    public void emparcharUnaCuenta(Cuenta parche) {
        final Query query = query(where("_id").is(parche.numero));
        final Update update = new Update();

        set(update,"moneda",parche.moneda);
        set(update,"saldo",parche.saldo);
        set(update,"tipo",parche.tipo);
        set(update,"estado",parche.estado);
        set(update,"oficina",parche.oficina);
        System.err.println("************************************************");

        mongoOperations.updateFirst(query,update,"cuenta");

        //if(parche.numero == null)
        //    throw new IllegalArgumentException("Falta número de cuenta.");

        /*final Cuenta existente = this.cuentas.get(parche.numero);

        if(parche.moneda != existente.moneda)
            existente.moneda = parche.moneda;

        if(parche.estado != null)
            existente.estado = parche.estado;

        if (parche.saldo != existente.saldo)
            existente.saldo = parche.saldo;

        if (parche.oficina != null)
            existente.oficina = parche.oficina;

        if (parche.tipo != null)
            existente.tipo = parche.tipo;

        this.cuentas.replace(existente.numero, existente);
        */
    }
    private void set(Update update, String nombre, Object valor) {
        if(valor != null) {
            System.err.println(String.format("==== MODIFICAR CAMPO %s = %s", nombre, valor));
            update.set(nombre, valor);
        }
    }

    @Override
    public void borrarCuenta(String numero) {
        this.cuentas.remove(numero);
    }

    @Autowired
    ServicioCliente servicioCliente;

    @Override
    public Cuenta obtenerUnaCuentaUnCliente(String documento, String numeroCuenta) {
        if(!this.repositorioCuenta.existsById(numeroCuenta))
            throw new RuntimeException("No existe la cuenta número " + numeroCuenta);

        final var cliente = this.servicioCliente.obtenerUnCliente(documento);

        if(!cliente.codigosCuentas.contains(numeroCuenta))
            throw new RuntimeException("El cliente no tiene esta cuenta asociada: " + numeroCuenta);

        final var quizasCuenta = this.repositorioCuenta.findById(numeroCuenta);
        if(!quizasCuenta.isPresent())
            throw new RuntimeException("No existe la cuenta número " + numeroCuenta);

        return quizasCuenta.get();
    }

    @Override
    public void eliminarCuentaCliente(String documento, String numeroCuenta) {
        final var cliente = this.servicioCliente.obtenerUnCliente(documento);
        final var quizasCuenta = this.repositorioCuenta.findById(numeroCuenta);
        if(!quizasCuenta.isPresent())
            throw new RuntimeException("No existe la cuenta " + numeroCuenta);
        if(!cliente.codigosCuentas.contains(numeroCuenta))
            throw new RuntimeException("El cliente no tiene esta cuenta asociada: " + numeroCuenta);
        final var cuenta = quizasCuenta.get();
        cuenta.estado = "INACTIVA";
        cliente.codigosCuentas.remove(numeroCuenta);
        guardarCuenta(cuenta);
        this.servicioCliente.guardarCliente(cliente);

        System.err.println(String.format("==== eliminarCuentaCliente %s = %s ","documento", documento));
        System.err.println(String.format("==== eliminarCuentaCliente %s = %s ","numero", numeroCuenta));
        System.err.println("************************************************");
    }
}
