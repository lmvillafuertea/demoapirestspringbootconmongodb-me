package mis.pruebas.apirest.servicios.repositorios.impl;

import mis.pruebas.apirest.modelos.Cliente;
import mis.pruebas.apirest.modelos.CuentasCliente;
import mis.pruebas.apirest.servicios.repositorios.RepositorioClienteExtendido;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;
import static org.springframework.data.mongodb.core.query.Criteria.where;

public class RepositorioClienteExtendidoImpl implements RepositorioClienteExtendido {

    @Autowired
    MongoOperations mongoOperations;

    @Override
    public CuentasCliente obtenerCuentasCliente(String documento) {
        final var r =
                this.mongoOperations.aggregate(newAggregation(
                        /* 1 */ match(where("_id").is(documento)),
                        /* 2 */ lookup("cuenta","codigosCuentas","_id","cuentas"),
                        /* 3 */ addFields().addFieldWithValue("documento", "$_id").build(),
                        /* 4 */ project("documento", "nombre", "correo", "cuentas")
                ), Cliente.class, CuentasCliente.class);
        final var cuentasClientes = r.getMappedResults();
        if (cuentasClientes.size() > 0)
            return cuentasClientes.get(0);
        // Si no hay resultados -> objeto vacio.
        final var vacio = new CuentasCliente();
        vacio.documento = documento;
        return vacio;
    }
}
