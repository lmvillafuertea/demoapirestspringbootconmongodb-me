package mis.pruebas.apirest.servicios.repositorios;

import mis.pruebas.apirest.modelos.CuentasCliente;

public interface RepositorioClienteExtendido {

    public CuentasCliente obtenerCuentasCliente(String documento);

}
