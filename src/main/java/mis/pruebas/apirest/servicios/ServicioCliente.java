package mis.pruebas.apirest.servicios;

import mis.pruebas.apirest.modelos.Cliente;
import mis.pruebas.apirest.modelos.CuentasCliente;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ServicioCliente {

    // CRUD

    public List<Cliente> obtenerClientes(int pagina, int cantidad);
    //public Page<Cliente> obtenerClientes(int pagina, int cantidad);

    // CREATE
    public void insertarClienteNuevo(Cliente cliente);

    // READ
    public Cliente obtenerUnCliente(String documento);

    // UPDATE (solamente modificar, no crear).
    public void guardarCliente(Cliente cliente);
    public void emparcharUnCliente(Cliente parche);

    // DELETE
    public void borrarCliente(String documento);


    public void agregarCuentaCliente(String documento, String numeroCuenta);

    //public List<String> obtenerCuentasCliente(String documento);
    public CuentasCliente obtenerCuentasCliente(String documento);
}
