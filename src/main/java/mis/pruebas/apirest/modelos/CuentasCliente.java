package mis.pruebas.apirest.modelos;

import java.util.List;

public class CuentasCliente {
    public String documento, nombre, correo;
    public List<Cuenta> cuentas;
}
